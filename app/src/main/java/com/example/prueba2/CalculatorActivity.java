package com.example.prueba2;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.content.Intent;


public class CalculatorActivity extends AppCompatActivity {

    private TextView welcomeTextView;
    private EditText number1EditText;
    private EditText number2EditText;
    private Button sumaButton;

    private Button restaButton;
    private TextView resultTextView;

    private Button multiButton;

    private Button divButton;

    private Button btnLimpiar;

    private Button btnRegresar;




protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_calculator);

    welcomeTextView = findViewById(R.id.welcomeTextView);
    number1EditText = findViewById(R.id.number1EditText);
    number2EditText = findViewById(R.id.number2EditText);
    sumaButton = findViewById(R.id.sumaButton);
    resultTextView = findViewById(R.id.resultTextView);
    restaButton = findViewById(R.id.restaButton);
    multiButton = findViewById(R.id.multiButton);
    divButton = findViewById(R.id.divButton);
    btnLimpiar = findViewById(R.id.btnLimpiar);
    btnRegresar = findViewById(R.id.btnRegresar);

    Intent intent = getIntent();
    if (intent != null) {
        String username = intent.getStringExtra("username");
        welcomeTextView.setText("¡Hola, " + username + "!");
    }
    btnRegresar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    });
    btnLimpiar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            number1EditText.setText("");
            number2EditText.setText("");
            resultTextView.setText("");
        }
    });
    divButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            double num1 = Double.parseDouble(number1EditText.getText().toString());
            double num2 = Double.parseDouble(number2EditText.getText().toString());
            double resultado = num1 / num2;
            resultTextView.setText("Resultado: " + resultado);

        }
    });

    multiButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            double num1 = Double.parseDouble(number1EditText.getText().toString());
            double num2 = Double.parseDouble(number2EditText.getText().toString());
            double resultado = num1 * num2;
            resultTextView.setText("Resultado: " + resultado);

        }
    });
    restaButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            double num1 = Double.parseDouble(number1EditText.getText().toString());
            double num2 = Double.parseDouble(number2EditText.getText().toString());
            double resultado = num1 - num2;
            resultTextView.setText("Resultado: " + resultado);
        }
    });

    sumaButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            double num1 = Double.parseDouble(number1EditText.getText().toString());
            double num2 = Double.parseDouble(number2EditText.getText().toString());
            double resultado = num1 + num2;
            resultTextView.setText("Resultado: " + resultado);
        }

    });
}
}
